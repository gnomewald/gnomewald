#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <SDL2/SDL.h>
#include "die.h"
#include "exit-hooks.h"
#include "geometry.h"
#include "used.h"
#include "version.h"
#include "macros.h"
#include "critalloc.h"
#include "unit-list.h"
#include "unit.h"
#include "job.h"
#include "map.h"
#include "direction.h"
#include "search.h"
#include "options.h"

/**
 * @defgroup main Main (main.c)
 * Running the program
 * @{
 */

/**
 * The map data (0 = floor, 1 = wall).
 */
const int map_data[] = {
    1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
    0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1,
    1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
    0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1,
    1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1,
    1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
    1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1,
    1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1
};

/**
 * The width of the map.
 */
#define MAP_WIDTH 20

/**
 * The height of the map.
 */
#define MAP_HEIGHT 15

/**
 * Draw a rectangle.
 *
 * @param renderer  the SDL_Renderer to draw with
 * @param x  the x coordinate of the rectangle
 * @param y  the y coordinate of the rectangle
 * @param w  the width of the rectangle
 * @param h  the height of the rectangle
 */
void fill_rect(SDL_Renderer *renderer, int x, int y, int w, int h) {
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = w;
    r.h = h;
    SDL_RenderFillRect(renderer, &r);
}

/**
 * Stroke a rectangle.
 *
 * @param renderer  the SDL_Renderer to draw with
 * @param x  the x coordinate of the rectangle
 * @param y  the y coordinate of the rectangle
 * @param w  the width of the rectangle
 * @param h  the height of the rectangle
 */
void stroke_rect(SDL_Renderer *renderer, int x, int y, int w, int h) {
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = w;
    r.h = h;
    SDL_RenderDrawRect(renderer, &r);
}

/**
 * Make a unit execute its current job.
 *
 * @param unit  the unit
 */
void execute_job(Map *map, Unit *unit) {
    Agenda *agenda = unit->current_agenda;

    if (agenda->job_type == JOB_GO_TO_UNIT) {
        JobGoToUnit *job = &agenda->job.go_to_unit;

        /* Only units that can move should have the "go to unit" job. */
        SDL_assert(unit->tags & UNIT_CAN_MOVE);

        /* If the unit has reached its goal, do the next job. */
        if (points_equal(&unit->position, &job->target->position)
            || job->steps_taken == job->path_length) {
            clean_agenda(unit->current_agenda);
            unit->current_agenda = agenda->next;
            init_agenda(map, unit, unit->current_agenda);
            execute_job(map, unit);
        } else {
            int x_offset, y_offset, direction;

            /* Recalculate the path if necessary. */
            if (!points_equal(&job->target->position,
                              &job->last_known_pos)) {
                clean_agenda(unit->current_agenda);
                init_agenda(map, unit, unit->current_agenda);
            }

            if (job->path_length > 0) {
                int nx, ny;

                /* Step toward the destination. */
                direction = job->path[job->steps_taken];
                direction_as_offset(direction, &x_offset, &y_offset);
                nx = unit->position.x + x_offset;
                ny = unit->position.y + y_offset;

                /* bug - what happens when the unit's path was obstructed? */
                if (!is_tile_walkable_on_map(map, nx, ny)) {
                    SDL_assert(0);
                }

                move_point(&unit->position, x_offset, y_offset);
                job->steps_taken++;

                /* Start the unit's animation. */
                unit->animation.direction = direction;
                unit->animation.progress = 0;
            } else {
                /* We have reached the destination, so stop moving. */
                clean_agenda(unit->current_agenda);
                unit->current_agenda = agenda->next;
                init_agenda(map, unit, unit->current_agenda);
                execute_job(map, unit);
            }
        }
    } else if (agenda->job_type == JOB_HIT) {
        int direction, dx, dy;
        Unit *target = agenda->job.hit.target;

        dx = unit->position.x - target->position.x;
        dy = unit->position.y - target->position.y;

        /* Ensure that the target is still in range. */
        if (dx < -1 || dx > 1 || dy < -1 || dy > 1) {
            /* The target has gone out of range, so quit the job. */
            unit->current_agenda = agenda->next;
        } else {
            target->scores.health -= unit->fighting.strength;
            unit->current_agenda = agenda->next;
            unit_notify_attacked(target, unit);

            /* Start the unit's animation. */
            direction = offset_as_direction(dx, dy);
            unit->animation.direction = direction;
            unit->animation.progress = ANIM_PROGRESS_NEEDED / 3 * 2;
        }
    } else {
        /* Unknown job type---raise an error! */
        SDL_assert(0);
    }

    /* If the unit has no current agenda but still has an agenda list,
     * free the agenda list. */
    if (unit->agenda_list && unit->current_agenda == NULL) {
        unit_clear_agenda(unit);
    }
}

/**
 * Entrypoint of the program.
 *
 * @param argc  the number of command-line arguments
 * @param argv  the array of command-line arguments
 * @return  the program exit status
 */
int main(int argc, char **argv) {
    Map map;
    SDL_Window *window;
    SDL_Renderer *renderer;
    UnitList units;
    int wave_timer = 0;
    struct {
        Point start;
        Point end;
    } selection;
    int selecting = 0;

    USED(argc);
    USED(argv);

    /* Initialize the exit hooks system. */
    init_exit_hooks();

    /* Initialize SDL. */
    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        die("SDL_Init: %s\n", SDL_GetError());
    }
    add_exit_hook(&SDL_Quit);

    /* Open the window. */
    window = SDL_CreateWindow("Gnomewald " VERSION,
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              640, 480, 0);
    if (!window) {
        die("SDL_CreateWindow: %s\n", SDL_GetError());
    }
    add_exit_hook_with_arg(&SDL_DestroyWindow, window);

    /* Create the renderer. */
    renderer = SDL_CreateRenderer(window, -1, 0);
    if (!renderer) {
        die("SDL_CreateRenderer: %s\n", SDL_GetError());
    }
    add_exit_hook_with_arg(&SDL_DestroyRenderer, renderer);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    /* Initialize the map. */
    init_map(&map, (int*)map_data, MAP_WIDTH, MAP_HEIGHT, &units);

    /* Initialize the units. */
    unit_list_init(&units);
    add_exit_hook_with_arg(&unit_list_clear, &units);
    unit_list_add(
      &units,
      unit_new(8, 1, 1, 1, 255, 0, 0, 4,
               UNIT_CAN_DIG | UNIT_IS_DRAWN
               | UNIT_CAN_MOVE | UNIT_CAN_FIGHT));
    unit_list_add(
      &units,
      unit_new(5, 1, 5, 1, 255, 0, 255, 4,
               UNIT_CAN_DIG | UNIT_IS_DRAWN |
               UNIT_CAN_MOVE | UNIT_CAN_FIGHT));
    unit_list_add(
      &units,
      unit_new(7, 2, 16, 7, 0, 0, 255, 4,
               UNIT_IS_DRAWN | UNIT_CAN_MOVE
               | UNIT_IS_EVIL | UNIT_CAN_FIGHT));

    /* Run the main loop. */
    while (1) {
        ULIter iter;
        Unit *unit;
        int i;
        SDL_Event event;

        /* Handle user input. */
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                exit(EXIT_SUCCESS);
            } else if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (event.button.button == SDL_BUTTON_LEFT) {
                    printf("Press\n");
                    set_point(&selection.start,
                              event.button.x / TILE_SIZE,
                              event.button.y / TILE_SIZE);
                    set_point(&selection.end,
                              event.button.x / TILE_SIZE,
                              event.button.y / TILE_SIZE);
                    selecting = 1;
                }
            } else if (event.type == SDL_MOUSEBUTTONUP) {
                if (event.button.button == SDL_BUTTON_LEFT) {
                    printf("Release\n");
                    selecting = 0;
                }
            } else if (event.type == SDL_MOUSEMOTION) {
                if (selecting) {
                    set_point(&selection.end,
                              event.motion.x / TILE_SIZE,
                              event.motion.y / TILE_SIZE);
                }
            }
        }

        /* Get rid of any dead units. */
        while (1) {
            int dead_found = 0;

            unit_list_iter_start(map.units, &iter);
            unit = unit_list_iter_next(&iter);
            while (unit) {
                if (unit->scores.health <= 0) {
                    Unit *other;
                    ULIter other_iter;

                    dead_found = 1;

                    /* Make sure no units consider the dead unit
                     * to be their opponent. */
                    unit_list_iter_start(map.units, &other_iter);
                    other = unit_list_iter_next(&other_iter);
                    while (other) {
                        if (other->opponent == unit) {
                            other->opponent = NULL;
                        }
                        other = unit_list_iter_next(&other_iter);
                    }

                    /* Delete the dead unit. */
                    unit_list_delete(&units, unit);
                    break;
                }
                unit = unit_list_iter_next(&iter);
            }

            if (!dead_found) {
                break;
            }
        }

        /* Update the state of all living units. */
        unit_list_iter_start(&units, &iter);
        unit = unit_list_iter_next(&iter);
        while (unit) {
            if (unit->animation.progress >= ANIM_PROGRESS_NEEDED) {
                if (unit->current_agenda) {
                    /* The unit is not animated or idling,
                     * so make it do its job. */
                    execute_job(&map, unit);
                } else {
                    if (unit->tags & UNIT_CAN_FIGHT) {
                        /* If the unit can fight, tell it to attack
                         * an enemy. */
                        if (unit->tags & UNIT_CAN_MOVE) {
                            Unit *target = NULL;

                            /* If the unit already has an opponent,
                             * then fight that one. */
                            if (unit->opponent) {
                                target = unit->opponent;
                            } else {
                                Unit *other;
                                ULIter otheriter;

                                /* Find an enemy to walk to. */
                                unit_list_iter_start(&units, &otheriter);
                                other = unit_list_iter_next(&otheriter);
                                while (other) {
                                    if (other == unit) {
                                        other = unit_list_iter_next(&otheriter);
                                        continue;
                                    }

                                    /* bug - what if the unit is unreachable? */
                                    if ((other->tags & UNIT_IS_EVIL)
                                          != (unit->tags & UNIT_IS_EVIL)
                                        && other->scores.health > 0) {
                                        target = other;
                                        unit->opponent = other;
                                        break;
                                    }

                                    other = unit_list_iter_next(&otheriter);
                                }
                            }

                            if (target) {
                                unit->agenda_list = new_fight_agenda_list(target);
                                unit->current_agenda = unit->agenda_list;
                                init_agenda(&map, unit, unit->current_agenda);
                            }
                        } else {
                            /* Attack an enemy who is already near. */
                            /* todo - this is not yet implemented */
                            SDL_assert(0);
                        }
                    }
                }
            } else {
                /* Continue the unit's animation. */
                unit->animation.progress += unit->speed.speed;
                if (unit->animation.progress > ANIM_PROGRESS_NEEDED) {
                    unit->animation.progress =
                        ANIM_PROGRESS_NEEDED;
                }
            }

            unit = unit_list_iter_next(&iter);
        }

        /* Draw the scene. */
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderClear(renderer);

        /* ... Draw the map. */
        for (i = 0; i < MAP_HEIGHT; i++) {
            int j;
            for (j = 0; j < MAP_WIDTH; j++) {
                int tile = map.data[i * MAP_WIDTH + j];

                if (tile == 0) {
                    SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
                } else if (tile == 1) {
                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
                }

                fill_rect(renderer,
                          j * TILE_SIZE, i * TILE_SIZE,
                          TILE_SIZE, TILE_SIZE);
            }
        }

        /* ... Draw the units. */
        unit_list_iter_start(&units, &iter);
        unit = unit_list_iter_next(&iter);
        while (unit) {
            if (unit->tags & UNIT_IS_DRAWN) {
                float progress_distance;

                SDL_SetRenderDrawColor(renderer,
                                       unit->color.r,
                                       unit->color.g,
                                       unit->color.b,
                                       255);

                /* Calculate how far removed the unit is from
                 * its true location. */
                progress_distance =
                    (1.0f
                     - ((float)unit->animation.progress
                        / (float)ANIM_PROGRESS_NEEDED))
                    * (float)TILE_SIZE;

                /* Draw a filled square at the unit's animated
                 * location, which may be offset from its
                 * true location. */
                if (unit->animation.progress < ANIM_PROGRESS_NEEDED) {
                    int direction_offset_x;
                    int direction_offset_y;
                    int move_offset_x;
                    int move_offset_y;

                    direction_as_offset(unit->animation.direction,
                                        &direction_offset_x,
                                        &direction_offset_y);

                    move_offset_x = (int)(-direction_offset_x
                                          * progress_distance);
                    move_offset_y = (int)(-direction_offset_y
                                          * progress_distance);

                    fill_rect(renderer,
                              unit->position.x * TILE_SIZE + 2
                                + move_offset_x,
                              unit->position.y * TILE_SIZE + 2
                                + move_offset_y,
                              TILE_SIZE - 4, TILE_SIZE - 4);
                } else {
                    fill_rect(renderer,
                              unit->position.x * TILE_SIZE + 2,
                              unit->position.y * TILE_SIZE + 2,
                              TILE_SIZE - 4, TILE_SIZE - 4);

                }

                /* Draw an outlined square at the unit's
                 * true location. */
                stroke_rect(renderer,
                            unit->position.x * TILE_SIZE,
                            unit->position.y * TILE_SIZE,
                            TILE_SIZE, TILE_SIZE);
            }

            unit = unit_list_iter_next(&iter);
        }

        /* ... Draw the selection box. */
        if (selecting) {
            int min_x, min_y, max_x, max_y;

            if (selection.start.x <= selection.end.x) {
                min_x = selection.start.x;
                max_x = selection.end.x;
            } else {
                min_x = selection.end.x;
                max_x = selection.start.x;
            }

            if (selection.start.y <= selection.end.y) {
                min_y = selection.start.y;
                max_y = selection.end.y;
            } else {
                min_y = selection.end.y;
                max_y = selection.start.y;
            }

            SDL_SetRenderDrawColor(renderer, 255, 255, 0, 200);
            fill_rect(renderer,
                      min_x * TILE_SIZE,
                      min_y * TILE_SIZE,
                      (max_x - min_x + 1) * TILE_SIZE,
                      (max_y - min_y + 1) * TILE_SIZE);
        }

        /* Refresh the display. */
        SDL_RenderPresent(renderer);

        /* Delay until the next frame. */
        wave_timer++;
        if (wave_timer == WAVE_INTERVAL) {
            unit_list_add(
             &units,
             unit_new(7, 2, MAP_WIDTH - 1, 2, 0, 0, 255, 4,
                      UNIT_IS_DRAWN | UNIT_CAN_MOVE
                      | UNIT_IS_EVIL | UNIT_CAN_FIGHT));
            wave_timer = 0;
        }
        SDL_Delay(30);
    }

    return 0;
}

/** @} */

