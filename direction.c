#include "direction.h"
#include <SDL2/SDL.h>

int offset_as_direction(int x_offset, int y_offset) {
    SDL_assert(x_offset >= -1 && x_offset <= 1);
    SDL_assert(y_offset >= -1 && y_offset <= 1);

    if (x_offset == -1 && y_offset == -1) {
        return NORTHEAST;
    } else if (x_offset == 0 && y_offset == -1) {
        return NORTH;
    } else if (x_offset == 1 && y_offset == -1) {
        return NORTHWEST;
    } else if (x_offset == -1 && y_offset == 0) {
        return EAST;
    } else if (x_offset == 0 && y_offset == 0) {
        return CENTER;
    } else if (x_offset == 1 && y_offset == 0) {
        return WEST;
    } else if (x_offset == -1 && y_offset == 1) {
        return SOUTHEAST;
    } else if (x_offset == 0 && y_offset == 1) {
        return SOUTH;
    } else if (x_offset == 1 && y_offset == 1) {
        return SOUTHWEST;
    }

    return -1;
}

void direction_as_offset(int direction, int *x_offset, int *y_offset) {
    switch (direction) {
        case NORTHEAST:
            *x_offset = -1;
            *y_offset = -1;
            break;
        case NORTH:
            *x_offset = 0;
            *y_offset = -1;
            break;
        case NORTHWEST:
            *x_offset = 1;
            *y_offset = -1;
            break;
        case EAST:
            *x_offset = -1;
            *y_offset = 0;
            break;
        case CENTER:
            *x_offset = 0;
            *y_offset = 0;
            break;
        case WEST:
            *x_offset = 1;
            *y_offset = 0;
            break;
        case SOUTHEAST:
            *x_offset = -1;
            *y_offset = 1;
            break;
        case SOUTH:
            *x_offset = 0;
            *y_offset = 1;
            break;
        case SOUTHWEST:
            *x_offset = 1;
            *y_offset = 1;
            break;
        default:
            SDL_assert(0);
            break;
    }
}

