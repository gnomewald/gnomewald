#include "critalloc.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "die.h"

void *critmalloc(size_t size) {
    void *ptr = malloc(size);

    if (!ptr) {
        die("error: malloc: %s\n", strerror(errno));
    }

    return ptr;
}

void *critrealloc(void *ptr, size_t size) {
    void *new_ptr = realloc(ptr, size);

    if (!new_ptr) {
        die("error: realloc: %s\n", strerror(errno));
    }

    return new_ptr;
}
