#ifndef SEARCH_H
#define SEARCH_H
#include "map.h"

/** @defgroup search Search (search.c, search.h)
 *  Breadth-first searching for a goal on the map.
 *  @{
 */

/**
 * Search for a goal via a breadth-first search.
 *
 * @param map  the map
 * @param start_x  the x coordinate of the starting location
 * @param start_y  the y coordinate of the starting location
 * @param goal_x  the x coordinate of the goal location
 * @param goal_y  the y coordinate of the goal location
 * @param path_length  the pointer to store the path length at
 * @return  the path to the goal, e.g., {NORTH, NORTHWEST},
 *          or NULL if unreachable
 * @note  The caller is responsible for freeing the returned path
 *        even if the path's length is 0.
 */
extern int *search(struct Map *map,
                   int start_x, int start_y,
                   int goal_x, int goal_y,
                   int *path_length);


/** @} */

#endif /* SEARCH_H */
