#ifndef DIRECTION_H
#define DIRECTION_H

/** @defgroup directions Directions (direction.c, direction.h)
 *  Converting from directions to offsets and vise versa.
 *  @{
 */

/** An enum defining the compass directions. */
enum Direction {
    NORTHEAST,
    NORTH,
    NORTHWEST,
    EAST,
    CENTER,
    WEST,
    SOUTHEAST,
    SOUTH,
    SOUTHWEST
};

/**
 * Convert a pair of offset coordinates into a direction.
 *
 * @param x_offset  the offset on the x-axis (must be -1, 0, or 1)
 * @param y_offset  the offset on the y-axis (must be -1, 0, or 1)
 * @return  the direction (see #Direction)
 */
extern int offset_as_direction(int x_offset, int y_offset);

/**
 * Convert a direction into a pair of offset coordinates.
 *
 * @param direction  the direction (see #Direction)
 * @param x_offset  a pointer to where the x offset will be stored
 * @param y_offset  a pointer to where the y offset will be stored
 */
extern void direction_as_offset(int direction,
                                int *x_offset,
                                int *y_offset);

/** @} */

#endif /* DIRECTION_H */

