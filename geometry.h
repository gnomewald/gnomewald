#ifndef GEOMETRY_H
#define GEOMETRY_H

/** @defgroup geometry Geometry (geometry.c, geometry.h)
 *  Functions for doing geometric math
 *  @{
 */

/** A Point represents a location in 2D space. */
struct Point {
    int x; /** The x coordinate of the point. */
    int y; /** The y coordinate of the point. */
};
typedef struct Point Point;

/**
 * Move a point's coordinates.
 *
 * @param pt  a pointer to the point
 * @param x_offset  the amount to move the x coordinate by
 * @param y_offset  the amount to move the y coordinate by
 */
extern void move_point(Point *pt, int x_offset, int y_offset);

/**
 * Return whether two points are equal.
 *
 * @param a  a pointer to the first point
 * @param b  a pointer to the second point
 * @return  1 if the points are equal, otherwise 0
 */
extern int points_equal(Point *a, Point *b);

/**
 * Set the coordinates of a point.
 * 
 * @param result  a pointer to the point
 * @param x  the x coordinate
 * @param y  the y coordinate
 */
extern void set_point(Point *result, int x, int y);

/**
 * Return the distance between two points.
 *
 * @param a  the first point
 * @param b  the second point
 * @return  the distance between the points
 */
extern double points_distance(Point *a, Point *b);

/** @} */

#endif /* GEOMETRY_H */

