#include "unit.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "critalloc.h"
#include "geometry.h"
#include "options.h"

Unit *unit_new(int health, int strength,
               int x, int y,
               int r, int g, int b,
               int speed, int tags) {
    Unit *unit = critmalloc(sizeof *unit);
    memset(unit, 0, sizeof *unit);

    unit->scores.health = health;
    unit->fighting.strength = strength;
    set_point(&unit->position, x, y);
    unit->color.r = r;
    unit->color.g = g;
    unit->color.b = b;
    unit->speed.speed = speed;
    unit->tags = tags;

    unit->animation.progress = ANIM_PROGRESS_NEEDED;
    unit->agenda_list = NULL;
    unit->current_agenda = NULL;
    unit->opponent = NULL;

    return unit;
}

void unit_clear_agenda(Unit *unit) {
    if (unit->current_agenda) {
        clean_agenda(unit->current_agenda);
        unit->current_agenda = NULL;
    }

    if (unit->agenda_list) {
        free_agenda_list(unit->agenda_list);
        unit->agenda_list = NULL;
    }
}

void unit_free(Unit *unit) {
    unit_clear_agenda(unit);
    free(unit);
}

void unit_notify_attacked(Unit *unit, Unit *attacker) {
    Agenda *agenda;

    /* If the unit is not already in combat with an opponent,
     * or if the attacker is closer that the unit's current opponent,
     * then set the unit's opponent to be the attacker. */
    if (!unit->opponent
        || points_distance(&unit->position,
                           &attacker->position)
           < points_distance(&unit->position,
                             &unit->opponent->position)) {
        unit->opponent = attacker;

        /* Clear the unit's agenda so that it can start fighting. */
        agenda = unit->current_agenda;
        if (agenda) {
            unit_clear_agenda(unit);
        }
    }
}
