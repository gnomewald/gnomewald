#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "die.h"
#include "critalloc.h"
#include "exit-hooks.h"

struct _ExitHook {
    union {
        void (*with_no_arg)(void);
        void (*with_arg)(void*);
    } proc;
    void *data;
    int has_arg;
};

static struct _ExitHook *_exit_hooks = NULL;
static int _exit_hooks_count = 0;

static struct _ExitHook *_alloc_exit_hook(void) {
    struct _ExitHook *hook;

    _exit_hooks_count++;
    _exit_hooks =
      (struct _ExitHook*)critrealloc(_exit_hooks,
                                     _exit_hooks_count
                                     * sizeof(*_exit_hooks));

    hook = &_exit_hooks[_exit_hooks_count - 1];

    return hook;
}

void add_raw_exit_hook(void (*no_arg_proc)(void),
                       void (*with_arg_proc)(void*),
                       int has_arg,
                       void *data) {
    struct _ExitHook *hook = _alloc_exit_hook();
    hook->data = data;
    hook->has_arg = has_arg;
    if (has_arg) {
        hook->proc.with_arg = with_arg_proc;
    } else {
        hook->proc.with_no_arg = no_arg_proc;
    }
}

static void _run_and_free_exit_hooks(void) {
    int i;

    for (i = _exit_hooks_count - 1; i >= 0; i--) {
        struct _ExitHook *hook = &_exit_hooks[i];

        if (hook->has_arg) {
            hook->proc.with_arg(hook->data);
        } else {
            hook->proc.with_no_arg();
        }
    }

    if (_exit_hooks) {
        free(_exit_hooks);
        _exit_hooks = NULL;
    }
    _exit_hooks_count = 0;
}

void init_exit_hooks(void) {
    atexit(&_run_and_free_exit_hooks);
}

