#ifndef USED_H
#define USED_H

/** @defgroup used Used (used.h)
 *  Marking variables as having been used.
 *  @{
 */

/**
 * Mark a variable as having been used.
 *
 * @param x  the variable
 */
#define USED(x) ((void)(x))

#endif /* USED_H */

