#ifndef VERSION_H
#define VERSION_H

/** @defgroup version Version (version.h)
 *  Information about the game version.
 *  @{
 */

/**
 * The version of the game.
 */
#define VERSION "0.1.0-beta"

/** @} */

#endif /* VERSION_H */

