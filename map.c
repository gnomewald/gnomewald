#include "map.h"
#include <SDL2/SDL.h>
#include "unit.h"
#include "unit-list.h"

/**
 * Return whether a tile is solid or not.
 *
 * @param tile
 * @return 1 if the tile is solid, otherwise 0
 */
static int _is_tile_solid(int tile) {
    if (tile == 0) {
        return 0;
    } else {
        return 1;
    }
}

void init_map(Map *map,
              int *map_data,
              int width,
              int height,
              struct UnitList *units) {
    SDL_assert(width > 0);
    SDL_assert(height > 0);

    map->width = width;
    map->height = height;
    map->units = units;
    map->data = map_data;
}

/**
 * Return whether a tile is walkable on a map.
 *
 * If a tile is non-solid and there is not a unit already on it,
 * then the tile is considered walkable.
 *
 * @param map  the map
 * @param x  the x coordinate of the tile
 * @param y  the y coordinate of the tile
 * @return  1 if the tile is walkable, otherwise 0
 */
int is_tile_walkable_on_map(Map *map, int x, int y) {
    Unit *unit;
    ULIter iter;
    SDL_assert(x >= 0 && x < map->width);
    SDL_assert(y >= 0 && y < map->height);

    /* Check whether the tile is solid. */
    if (_is_tile_solid(map->data[y * map->width + x])) {
        /* The tile is solid, so it is not walkable. */
        return 0;
    }

    /* Check whether there is a unit on the tile. */
    unit_list_iter_start(map->units, &iter);
    unit = unit_list_iter_next(&iter);
    while (unit) {
        if (x == unit->position.x
            && y == unit->position.y) {
            /* There is a unit standing on the tile,
             * so it is not walkable. */
            return 0;
        }

        unit = unit_list_iter_next(&iter);
    }

    /* The tile is walkable. */
    return 1;
}

