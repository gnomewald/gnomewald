#ifndef OPTIONS_H
#define OPTIONS_H

/** @defgroup options Options (options.h)
 *  Configurable constants which affect game-play.
 *  @{
 */

/**
 * The size of tiles (in pixels).
 */
#define TILE_SIZE 32

/**
 * The amount of progress needed to complete an animation.
 */
#define ANIM_PROGRESS_NEEDED 40

/**
 * The amount of frames it takes for an enemy wave to arrive.
 */
#define WAVE_INTERVAL 300

/** @} */

#endif /* OPTIONS_H */

