#include "job.h"
#include <SDL2/SDL.h>
#include "critalloc.h"
#include "geometry.h"
#include "macros.h"
#include "search.h"

void clean_agenda(struct Agenda *agenda) {
    if (agenda->job_type == JOB_GO_TO_UNIT) {
        JobGoToUnit *job = &agenda->job.go_to_unit;
        if (job->path) {
            free(job->path);
            job->path = NULL;
        }
    }
}

void init_agenda(struct Map *map,
                 struct Unit *unit,
                 struct Agenda *agenda) {
    Unit *target;

    /* If the agenda is NULL, then the unit has reached
     * the end of its agenda chain, and nothing needs to be
     * initialized. */
    if (!agenda) {
        return;
    }

    switch (agenda->job_type) {
        case JOB_GO_TO_UNIT: {
            JobGoToUnit *job = &agenda->job.go_to_unit;

            target = job->target;
            job->path =
                search(map,
                       unit->position.x,
                       unit->position.y,
                       target->position.x,
                       target->position.y,
                       &job->path_length);
            job->path_length--;

            /* bug - what if no path could be found? */
            if (!job->path) {
                SDL_assert(0);
            }

            job->steps_taken = 0;
            set_point(
              &job->last_known_pos,
              target->position.x,
              target->position.y);
        }
        default:
            break;
    }
}

Agenda *new_fight_agenda_list(struct Unit *target) {
    int i;
    Agenda *agendas[2];

    for (i = 0; i < NELEMS(agendas); i++) {
        agendas[i] = (Agenda*)critmalloc(sizeof **agendas);
    }

    agendas[0]->job.go_to_unit.target = target;
    agendas[0]->next = agendas[1];
    agendas[0]->job_type = JOB_GO_TO_UNIT;

    agendas[1]->job.hit.target = target;
    agendas[1]->next = NULL;
    agendas[1]->job_type = JOB_HIT;

    return agendas[0];
}

void free_agenda_list(struct Agenda *agenda_list) {
    Agenda *current, *next;

    current = agenda_list;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
}

