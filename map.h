#ifndef MAP_H
#define MAP_H
/* Forward-declare the Map struct since other structs depend on it. */
struct Map;
#include "unit-list.h"

/** @defgroup maps Maps (map.c, map.h)
 *  Structures and procedures for maps, i.e., the game world.
 *  @{
 */

/**
 * A map represents the in-game world.
 */
typedef struct Map Map;
struct Map {
    int width; /**< The width of the map (in tiles). */
    int height; /**< The height of the map (in tiles). */
    struct UnitList *units; /**< The list of units on the map. */
    int *data; /**< The tile data (as a row-major list). */
};

/**
 * Initialize a Map.
 *
 * @param map  the map
 * @param map_data  the tile data of the map (0 = ground, 1 = wall)
 * @param width  the width of the map (in tiles)
 * @param height  the height of the map (in tiles)
 * @param units  the list of units on the map
 */
extern void init_map(Map *map,
                     int *map_data,
                     int width,
                     int height,
                     struct UnitList *units);

/**
 * Return whether a tile is walkable on a map.
 *
 * If a tile is non-solid and there is not a unit already on it,
 * then the tile is considered walkable.
 *
 * @param map  the map
 * @param x  the x coordinate of the tile
 * @param y  the y coordinate of the tile
 * @return  1 if the tile is walkable, otherwise 0
 */
extern int is_tile_walkable_on_map(Map *map, int x, int y);

/** @} */

#endif /* MAP_H */

