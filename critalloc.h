#ifndef CRITALLOC_H
#define CRITALLOC_H
#include <stdlib.h>

/** @defgroup critalloc Critical Allocation (critalloc.c, critalloc.h)
 *  Allocating memory but aborting upon error.
 *  @{
 */

/**
 * Allocate memory, but abort if there is an error.
 *
 * This procedure is just like malloc(3) except that it aborts if
 * the memory could not be allocated.
 *
 * @param size  the number of bytes to allocate
 * @return  a pointer to the allocated data
 */
extern void *critmalloc(size_t size);

/**
 * Reallocate memory, but abort if there is an error.
 *
 * This procedure is just like realloc(3) except that it aborts if
 * the memory could not be allocated.
 *
 * @param ptr  the pointer to the data
 * @param size  the new size of the data
 * @return  a pointer to the reallocated data
 */
void *critrealloc(void *ptr, size_t size);

/** @} */

#endif /* CRITALLOC_H */
