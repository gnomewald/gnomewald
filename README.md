# Gnomewald
Gnomewald is a free and open source (GPLv3) colony-managing strategy game
similar to Rimworld or Dwarf Fortress.  In this game, you help a colony
of gnomes protect themselves against their enemies, the dreadful orcs.

Gnomewald comes with fully documented source code and aims to be easy
to use, learn, and develop.

The niche of colony-managing simulations has long been dominated by
proprietary games, and I feel that the genre deserves to be explored
and improved in an open way which can best be achieved with freely
available code.

The game is not complete yet, but feel free to enjoy what is already
here!

## Dependencies
Before trying to compile Gnomewald, you should have the following
installed:

* SDL 2.0.5
* meson 0.37.1
* ninja 1.7.2

Other versions of those packages might work, but those are the ones
I use for developing Gnomewald.

## Compiling
To prepare the code for compilation, navigate to the source tree
and run this command:

    meson build

Then, navigate to the build/ directory:

    cd build

Finally, start the compilation:

    ninja

## Documentation
To generate the code documentation, run `doxygen Doxyfile` in the root
of the source tree.  The HTML files will appear in the docs/html/
directory.

## Planned Features
Planned features include the following:

* Fighting
* Hunting
* Foraging
* Mining
* Mushroom farming
* Wall building
* Trap building
* Crafting

Additionally, the following aspects of the gnomes' lives will play
a role in the game:

* Gnome uniqueness (e.g., appearance, interests, and abilities)
* Eating
* Drinking
* Sleeping
* Skill developing

## License
Copyright © 2018 Chris Murphy.

Gnomewald is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gnomewald is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
