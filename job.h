#ifndef JOB_H
#define JOB_H
#include "unit.h"
#include "geometry.h"
#include "map.h"

/** @defgroup jobs Jobs (job.c, job.h)
 *  Structures and procedures for jobs which units can perform.
 *  @{
 */

/**
 * A JobType is a kind of job which a unit can hold (see #Unit.job).
 */
enum JobType {
    JOB_GO_TO_UNIT,
    JOB_HIT
};

/**
 * A JobHit is a job in which a unit hits another unit.
 */
typedef struct JobHit JobHit;
struct JobHit {
    struct Unit *target;
};

/**
 * A JobGoToUnit is a job in which a unit walks next to another unit.
 */
typedef struct JobGoToUnit JobGoToUnit;
struct JobGoToUnit {
    struct Unit *target;
    int steps_taken;
    int path_length;
    int *path;
    Point last_known_pos;
};

/**
 * An Agenda combines a job with its type and a follow-up agenda, if any.
 */
typedef struct Agenda Agenda;
struct Agenda {
    union {
        JobGoToUnit go_to_unit;
        JobHit hit;
    } job; /**< The job this agenda represents. */
    int job_type; /**< The type of job this agenda represents. */
    Agenda *next; /**< The agenda to perform
                       after this one is done. */
};

/**
 * Clean an agenda by freeing any data it owns.
 *
 * @param agenda  the agenda
 */
extern void clean_agenda(struct Agenda *agenda);

/**
 * Initialize an agenda.
 *
 * This should be used as soon as the unit starts the agenda.
 *
 * @param unit  the unit starting the agenda
 * @param agenda   the agenda
 */
extern void init_agenda(struct Map *map,
                        struct Unit *unit,
                        struct Agenda *agenda);

/**
 * Create an agenda list in which one unit goes to and fights another.
 *
 * @param target  the unit to fight
 */
extern Agenda *new_fight_agenda_list(struct Unit *target);

/**
 * Free an agenda list.
 *
 * @param agenda_list  the agenda list
 */
extern void free_agenda_list(struct Agenda *agenda_list);

/** @} */

#endif /* JOB_H */

