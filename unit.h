#ifndef UNIT_H
#define UNIT_H
#include "job.h"
#include "geometry.h"
#include "map.h"

/** @defgroup units Units (unit.c, unit.h)
 *  Structures and procedures for in-game units
 *  @{
 */

/** A UnitTag denotes a capability of a given unit (see #Unit.tags). */
enum UnitTag {
    UNIT_CAN_DIG = 1, /**< The unit can dig through mountains. */
    UNIT_IS_DRAWN = 2, /**< The unit is drawn as a colored square. */
    UNIT_CAN_MOVE = 4, /**< The unit can move from tile to tile. */
    UNIT_IS_EVIL = 8, /**< The unit is considered evil by gnomes. */
    UNIT_CAN_FIGHT = 16 /**< The unit can fight other units. */
};

/**
 * A Unit is a being in the game world, e.g., a gnome.
 */
typedef struct Unit Unit;
struct Unit {
    struct {
        int health; /**< The life force of the unit. */
    } scores; /**< The basic scores of the unit, e.g., health. */
    struct {
        int strength; /**< The damage dealt by the unit in combat. */
    } fighting; /**< The fighting stats of the unit. */
    Point position; /**< The position of the unit. */
    struct {
        int progress; /**< How far the unit has progress in its
                           animation. */
        int direction; /**< The direction of the unit's animation. */
    } animation; /**< The animation properties of the unit. */
    struct {
        int r; /**< The red component of the unit's color (0-255). */
        int g; /**< The green component of the unit's color (0-255). */
        int b; /**< The blue component of the unit's color (0-255). */
    } color; /**< The color of the unit. */
    struct {
        int speed; /**< The unit's movement speed. */
    } speed;
    Unit *opponent; /**< The unit's opponent in combat. */
    struct Agenda *agenda_list; /**< The root of the unit's agenda list. */
    struct Agenda *current_agenda; /**< The unit's current agenda. */
    int tags; /**< The bitfield of the unit's tags (see #UnitTag). */
};

/**
 * Clear a unit's agenda list and current agenda.
 *
 * @param unit  the unit
 */
extern void unit_clear_agenda(Unit *unit);

/**
 * Free a unit.
 *
 * @param unit  the unit
 */
extern void unit_free(Unit *unit);

/**
 * Create a new unit.
 *
 * @param health  the starting health of the unit
 * @param strength  the strength of the unit
 * @param x  the x coordinate of the unit's position
 * @param y  the y coordinate of the unit's position
 * @param r  the red component of the unit's color (0-255)
 * @param g  the green component of the unit's color (0-255)
 * @param b  the blue component of the unit's color (0-255)
 * @param speed  the unit's movement speed
 * @param tags  the unit's tags (see #UnitTag)
 * @note  the caller is responsible for freeing the unit
 */
extern Unit *unit_new(int health, int strength,
                      int x, int y,
                      int r, int g, int b,
                      int speed, int tags);
/**
 * Notify a unit that it was attacked.
 *
 * @param map  the map which the units exist in
 * @param unit  the unit which was attacked
 * @param attacker  the unit which performed the attack
 */
extern void unit_notify_attacked(struct Unit *unit,
                                 struct Unit *attacker);

/** @} */

#endif /* UNIT_H */

