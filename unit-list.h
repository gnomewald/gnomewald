#ifndef UNIT_LIST_H
#define UNIT_LIST_H
#include "unit.h"

/** @defgroup unit-lists Unit Lists (unit-list.c, unit-list.h)
 *  Maintaining and iterating over lists of units.
 *  @{
 */

/**
 * A ULNode is used internally by a UnitList to store units.
 */
typedef struct ULNode ULNode;
struct ULNode {
    struct Unit *unit;
    ULNode *next;
};

/**
 * A ULIter is used to iterate over a UnitList.
 */
typedef struct ULIter ULIter;
struct ULIter {
    ULNode *node;
};

/**
 * A UnitList stores and allows iteration over multiple units.
 */
typedef struct UnitList UnitList;
struct UnitList {
    ULNode *root;
    ULNode *end;
};

/**
 * Add a unit to a unit list.
 *
 * @param list  the list
 * @param unit  the unit
 */
extern void unit_list_add(UnitList *list, struct Unit *unit);

/**
 * Free and remove all units from a unit list.
 *
 * @param list  the unit list
 */
extern void unit_list_clear(UnitList *list);

/**
 * Free and remove a unit from a unit list.
 *
 * If the unit is not in the list, an assertion is triggered.
 *
 * @param list  the unit list
 * @param unit  the unit
 */
extern void unit_list_delete(UnitList *list, struct Unit *unit);

/**
 * Initialize a unit list.
 *
 * This must be called before using any other procedures on the list.
 *
 * @param list  the unit list
 */
extern void unit_list_init(UnitList *list);

/**
 * Return the next unit encountered by a unit list iterator.
 *
 * @param iter  the iterator
 * @return  the unit, or NULL if the end of the list was reached
 */
extern struct Unit *unit_list_iter_next(ULIter *iter);

/**
 * Set up a unit list iterator for the given unit list.
 *
 * @param list  the list
 * @param iter  the iterator
 */
extern void unit_list_iter_start(UnitList *list, ULIter *iter);

/** @} */

#endif /* UNIT_LIST_H */
