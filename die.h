#ifndef DIE_H
#define DIE_H

/** @defgroup die Die (die.c, die.h)
 *  Aborting the program with an error message.
 *  @{
 */

/**
 * Abort the program with an error message.
 *
 * @param fmt  the format specifier (see printf(3))
 * @param ...  the arguments to the format (see printf(3))
 */
extern void die(const char *fmt, ...);

/** @} */

#endif /* DIE_H */

