#ifndef MACROS_H
#define MACROS_H

/**
 * @defgroup macros Macros (macros.h)
 * Utility macros.
 * @{
 */

/**
 * Return the number of elements of a constant-size array.
 *
 * @param x  the array
 * @return  the number of elements
 */
#define NELEMS(x) (int)((sizeof (x)) / (sizeof *(x)))

/** @} */

#endif /* MACROS_H */
