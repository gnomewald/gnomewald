/* Include <stdlib.h> before this file. */
#ifndef EXIT_HOOKS_H
#define EXIT_HOOKS_H

/** @defgroup exit-hooks Exit Hooks (exit-hooks.c, exit-hooks.h)
 *  Running procedures when the program ends.
 *  @{
 */

/**
 * Add an exit hook with no arguments.
 *
 * The given procedure should have no return value and take
 * no arguments.
 *
 * @param proc  the procedure to run
 * @note If memory cannot be allocated for the new hook,
 *       the program will abort.
 */
#define add_exit_hook(proc) \
          add_raw_exit_hook((void(*)(void))(proc), NULL, 0, NULL)

/**
 * Add an exit hook with one argument.
 *
 * The given procedure should have no return value and take
 * one void pointer as an argument.
 *
 * @param proc  the procedure to run
 * @param data  the argument to pass to the procedure
 * @note If memory cannot be allocated for the new hook,
 *       the program will abort.
 */
#define add_exit_hook_with_arg(proc, data) \
          add_raw_exit_hook(NULL, (void(*)(void*))(proc), 1, (data))

/**
 * Add an exit hook with specific internal fields.
 *
 * You should use add_exit_hook and add_exit_hook_with_arg instead
 * of calling this directly.
 *
 * This is used by the add_exit_hook and add_exit_hook_with_arg macros.
 *
 * @param no_arg_proc  the value for the proc.no_arg field
 * @param with_arg_proc  the value for the proc.with_arg field
 * @param has_arg  the value for the has_arg field
 * @param data  the value for the has_arg field
 * @note If memory cannot be allocated for the new hook,
 *       the program will abort.
 */
extern void add_raw_exit_hook(void (*no_arg_proc)(void),
                              void (*with_arg_proc)(void*),
                              int has_arg,
                              void *data);

/**
 * Initialize the exit hooks system.
 *
 * This ensures that the hooks will be run before the program ends.
 */
extern void init_exit_hooks(void);

/** @} */

#endif /* EXIT_HOOKS_H */

