#include "search.h"
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "map.h"
#include "direction.h"
#include "critalloc.h"
#include "die.h"

int *search(struct Map *map,
            int start_x, int start_y,
            int goal_x, int goal_y,
            int *path_length) {
    int i, j, k;
    int *visited;
    int *breadcrumbs;
    int *frontier;
    int width = map->width;
    int height = map->height;

    SDL_assert(start_x >= 0 && start_x < width);
    SDL_assert(goal_x >= 0 && goal_y < height);

    if (start_x == goal_x && start_y == goal_y) {
        *path_length = 0;
        return (int*)critmalloc(sizeof(int));
    }

    /* Initialize the arrays for annotating the map. */
    visited = (int*)critmalloc(sizeof(int) * width * height);
    breadcrumbs = (int*)critmalloc(sizeof(int) * width * height);
    frontier = (int*)critmalloc(sizeof(int) * width * height);

    for (i = 0; i < width * height; i++) {
        visited[i] = 0;
        breadcrumbs[i] = CENTER;
        frontier[i] = 0;
    }

    /* The starting position has been explored. */
    visited[start_y * width + start_x] = 1;

    /* Mark the initial frontier. */
    for (j = -1; j < 2; j++) {
        for (k = -1; k < 2; k++) {
            int x = start_x + k;
            int y = start_y + j;

            if (k == 0 && j == 0) {
                continue;
            }

            if (x >= 0 && x < width && y >= 0 && y < height) {
                if ((x == goal_x && y == goal_y)
                    || is_tile_walkable_on_map(map, x, y)) {
                    breadcrumbs[y * width + x] =
                        offset_as_direction(k, j);
                    frontier[y * width + x] = 1;
                }
            }
        }
    }

    /* Keep pushing the frontier to find the goal. */
    while (1) {
        int frontier_found = 0;

        for (i = 0; i < width * height; i++) {
            if (frontier[i]) {
                int x, y;

                frontier_found = 1;

                /* Explore this tile. */
                x = i % width;
                y = i / width;
                if (x == goal_x && y == goal_y) {
                    /* We reached the goal, so build the path. */
                    int path_size = 0;
                    int *path = NULL;
                    int index, tmp;

                    /* Keep building until we reach the start. */
                    while (!(x == start_x && y == start_y)) {
                        int x_offset;
                        int y_offset;

                        /* Grow the path. */
                        path_size++;
                        path = critrealloc(path, path_size * sizeof(*path));

                        /* Record this part of the path. */
                        path[path_size - 1] =
                            breadcrumbs[y * width + x];

                        /* Step back along the path. */
                        direction_as_offset(
                            path[path_size - 1],
                            &x_offset,
                            &y_offset);
                        x -= x_offset;
                        y -= y_offset;
                    }

                    /* Reverse the path array. */
                    for (index = 0; index < path_size / 2; index++) {
                        tmp = path[index];
                        path[index] = path[path_size - 1 - index];
                        path[path_size - 1 - index] = tmp;
                    }

                    free(breadcrumbs);
                    free(frontier);
                    free(visited);
                    *path_length = path_size;
                    return path;
                } else {
                    /* Mark this tile as non-frontier and visited. */
                    frontier[y * width + x] = 0;
                    visited[y * width + x] = 1;

                    /* Mark any neighboring unexplored tiles as frontier. */
                    for (j = -1; j < 2; j++) {
                        for (k = -1; k < 2; k++) {
                            int nx = x + k;
                            int ny = y + j;

                            if (k == 0 && j == 0) {
                                continue;
                            }

                            if (nx >= 0 && nx < width
                                && ny >= 0 && ny < height) {
                                if (((nx == goal_x && ny == goal_y)
                                     || is_tile_walkable_on_map(map,
                                                                nx, ny))
                                    && !visited[ny * width + nx]
                                    && !frontier[ny * width + nx]) {
                                    breadcrumbs[ny * width + nx] =
                                        offset_as_direction(k, j);
                                    frontier[ny * width + nx] = 1;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!frontier_found) {
            break;
        }
    }

    free(breadcrumbs);
    free(frontier);
    free(visited);
    return NULL;
}

