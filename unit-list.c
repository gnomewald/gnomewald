#include "unit-list.h"
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "critalloc.h"

void unit_list_init(UnitList *list) {
    list->root = NULL;
    list->end = NULL;
}

void unit_list_add(UnitList *list, struct Unit *unit) {
    ULNode *node;

    node = critmalloc(sizeof *node);
    node->unit = unit;
    node->next = NULL;

    if (list->end) {
        list->end->next = node;
    } else {
        list->root = node;
    }

    list->end = node;
}

void unit_list_delete(UnitList *list, struct Unit *unit) {
    ULNode *prev = NULL;
    ULNode *node;

    /* Locate the unit in the unit list. */
    node = list->root;
    while (node && node->unit != unit) {
        prev = node;
        node = node->next;
    }

    /* Remove the unit from the list. */
    if (node) {
        if (prev == NULL) {
            list->root = node->next;
        } else {
            prev->next = node->next;
        }

        if (node == list->end) {
            list->end = prev;
        }

        /* Finally, free the node and its unit. */
        unit_free(node->unit);
        free(node);
    } else {
        /* The unit was not in the list --- fire an assertion. */
        SDL_assert(0);
    }
}

void unit_list_clear(UnitList *list) {
    ULNode *node, *next;

    node = list->root;
    while (node) {
        unit_free(node->unit);
        next = node->next;
        free(node);
        node = next;
    }

    list->root = NULL;
    list->end = NULL;
}

/**
 * Set up a unit list iterator for the given unit list.
 *
 * @param list  the list
 * @param iter  the iterator
 */
void unit_list_iter_start(UnitList *list, ULIter *iter) {
    iter->node = list->root;
}

/**
 * Return the next unit encountered by a unit list iterator.
 *
 * @param iter  the iterator
 * @return  the unit, or NULL if the end of the list was reached
 */
struct Unit *unit_list_iter_next(ULIter *iter) {
    Unit *unit;

    if (iter->node) {
        unit = iter->node->unit;
        iter->node = iter->node->next;
        return unit;
    } else {
        return NULL;
    }
}

