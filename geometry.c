#include "geometry.h"
#include <math.h>

void set_point(Point *result, int x, int y) {
    result->x = x;
    result->y = y;
}

void move_point(Point *pt, int x_offset, int y_offset) {
    pt->x += x_offset;
    pt->y += y_offset;
}

int points_equal(Point *a, Point *b) {
    return a->x == b->x && a->y == b->y;
}

double points_distance(Point *a, Point *b) {
    return sqrt(pow(fabs((double)(a->x - b->x)), 2.0)
                + pow(fabs((double)(a->y - b->y)), 2.0));
}

